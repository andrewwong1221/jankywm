#include <X11/Xlib.h>
#include <X11/extensions/Xrandr.h>
#include <X11/Xatom.h>

#define max(a, b) ((a) > (b) ? (a) : (b))
#define getKeySym(actual, alias)  KeyCode (alias) = XKeysymToKeycode(display, XStringToKeysym(actual));
#define MODKEY Mod4Mask 
#define TERM "xterm"
#define DMENU "dmenu_run"

/*  To give credit where credit is due: Monsterwm provided my study guide for their handling of button/key actions. Also, I read over the dwm source heavily for their handling of XEvents and floating movement 
    I was inspired by xmonad's choice of having the user set the cursor in their .xinitrc. 
*/


static Atom wmsignals[4];
static void killwindow(Display *display, Window w);

void spawnapp(char command[]) { 
    if (fork() == 0) { 
        setsid(); 
        execvp(command, NULL); }} 

void killwindow(Display *display, Window w) {
    XEvent closewin = { .type = ClientMessage };
    closewin.xclient.window = w;
    closewin.xclient.format = 32;
    closewin.xclient.message_type = wmsignals[0];
    closewin.xclient.data.l[0] = wmsignals[1];
    closewin.xclient.data.l[1] = CurrentTime;
    XSendEvent(display, w, False, NoEventMask, &closewin);
}
        
int main() {
    /*Xorg Biolerplate. XOpenDisplay grabs a lock on the display and returns the display
     struct that is used to interact with the current X session.
     
     The DefaultRootWindow returns a Window typedef for the XID(32-bit unsigned integer, of which only 29 bits are used, that names a unique object inside the server) of the root window. 
     */
    Display *display = XOpenDisplay(0x0);
    Window root = XDefaultRootWindow(display);
    
    /* Defined in dwm, and copied everywhere else, this array holds the atoms used for killing
     * and managing windows. */
    wmsignals[0] = XInternAtom(display, "WM_PROTOCOLS", False);
    wmsignals[1] = XInternAtom(display, "WM_DELETE_WINDOW", False);
    wmsignals[2] = XInternAtom(display, "WM_STATE", False);


    /* See XEvent description below. */
    XEvent event;
    
    /*

            typedef struct {
                int x, y;
                int width, height;
                int border_width;
                int depth;
                Visual *visual;
                Window root;
                int class;
                int bit_gravity;
                int win_gravity;
                int backing_store;
                unsigned long backing_planes;
                unsigned long backing_pixel;
                Bool save_under;
                Colormap colormap;
                Bool map_installed;
                int map_state;
                long all_event_masks;
                long your_event_mask;
                long do_not_propagate_mask;
                Bool override_redirect;
                Screen *screen;
            } XWindowAttributes;

      */
    
    XWindowAttributes win_attr;

    /* http://linux.die.net/man/3/xbuttonevent  */

    XButtonEvent buttonevent;
    
    /* Used to talk to the Xrandr library in order to broker screen sizes. The definitions are as follows: 
        XRRScreenSize *XRRSizes(Display *dpy, int screen, int *nsizes);
        XRRSizes returns the size and a pointer to the current sizes supported by the specified screen. 
        The first size specified is the default size of the server. If RandR is not supported, it returns 0 for the number of sizes.

        XRRGetScreenInfo Returns a screen configuration for later use; the information is private to the library. 
        Call XRRFreeScreenConfigInfo to free this information when you are finished with it. It forces a round trip to the server.

        The last line checks if the display value is non-null. If null, it returns an error.

    */

    int numberOfResolutions;
    XRRScreenSize *xrrs = XRRSizes(display, 0, &numberOfResolutions);
    
    XRRScreenConfiguration *screenconf = XRRGetScreenInfo(display, root);
    
    Rotation initialRotation;
    short initialRate = XRRConfigCurrentRate(screenconf);
    SizeID initial_size_id = XRRConfigCurrentConfiguration(screenconf, &initialRotation);
    XRRFreeScreenConfigInfo(screenconf);

    if (!(display)) return 1;

    /* Here is the place we define the keys we want to use for function. This macro expands to a call that grabs the key which creates the character that is the first arguement and gives it a globally-defined keyname that is the second argument. 
     
     The XGrabKey function takes a display pointer, a specific keycode or 'AnyKey' bitwise inclusive keymasks, which window is grabbed, a boolean which specifies whether keyboard events are treated normally, and then the pointer and keyboard modes. It establishes how the server is supposed to grab the keyboard.  

     XGrabButton takes the display, the button(1 for left, 2 for middle, 3 for right), the modkey to be used, the grab window, how the pointer events should be reported, the pointer bitmask, the pointer and keyboard grab mode, how its confined or not, and the cursor to be displayed. 
     */

    getKeySym("x", x)
    getKeySym("p", p)
    getKeySym("C", C)
    getKeySym("F", F)

    XGrabKey(display, AnyKey, MODKEY, root, True, GrabModeAsync, GrabModeAsync);
    XGrabButton(display, 1, MODKEY, root, True, ButtonPressMask, GrabModeAsync, GrabModeAsync, None, None);
    XGrabButton(display, 3, MODKEY, root, True, ButtonPressMask, GrabModeAsync, GrabModeAsync, None, None);
    XSelectInput(display, root, ButtonPressMask);
   
       
    /* Xnextevent returns the next XEvent union. XEvent unions are laid out like so:
    
    typedef union _XEvent {
    int type;  
    XAnyEvent xany;
    XKeyEvent xkey;
    XButtonEvent xbutton;
    XMotionEvent xmotion;
    XCrossingEvent xcrossing;
    XFocusChangeEvent xfocus;
    XExposeEvent xexpose;
    XGraphicsExposeEvent xgraphicsexpose;
    XNoExposeEvent xnoexpose;
    XVisibilityEvent xvisibility;
    XCreateWindowEvent xcreatewindow;
    XDestroyWindowEvent xdestroywindow;
    XUnmapEvent xunmap;
    XMapEvent xmap;
    XMapRequestEvent xmaprequest;
    XReparentEvent xreparent;
    XConfigureEvent xconfigure;
    XGravityEvent xgravity;
    XResizeRequestEvent xresizerequest;
    XConfigureRequestEvent xconfigurerequest;
    XCirculateEvent xcirculate;
    XCirculateRequestEvent xcirculaterequest;
    XPropertyEvent xproperty;
    XSelectionClearEvent xselectionclear;
    XSelectionRequestEvent xselectionrequest;
    XSelectionEvent xselection;
    XColormapEvent xcolormap;
    XClientMessageEvent xclient;
    XMappingEvent xmapping;
    XErrorEvent xerror;
    XKeymapEvent xkeymap;
    long pad[24];
    } XEvent;

    For types:
    http://www-h.eng.cam.ac.uk/help/tpl/graphics/X/X11R5/node23.html
   
    This essentially constantly reads the input from XNextEvent into the event pointer, and when it has a value, it executes the loop. This is a beautiful abstraction which I
    didn't quite understand until seeing it in the spectrwm source. Conformal++
     */
    
    while (!XNextEvent(display, &event)) {

        /*  These are the program launchers. event.type checks the type of Xorg event, and event.xkey.subwindow checks if 
            there is a child window. event.xkey.keycode checks the actual keycode as defined by our macros above. State is an 
            unsigned int which is the key or button mask.  
            
            http://linux.die.net/man/3/xkeyevent
            */
        if (event.type == KeyPress) {
           if (event.xkey.keycode == x && event.xkey.state & MODKEY) spawnapp(TERM);
           if (event.xkey.keycode == p && event.xkey.state & MODKEY) spawnapp(DMENU);
        }
        

        /* XRaiseWindow takes the child window and does what the name suggests. 
            XMoveResizeWindow takes the display, the subwindow, the x and y coordinates which show the window relative to its parent, and the width and height of the 
            new window. 

            This is actually the way to raise a window, or the first one is. 
            The second one makes the window fullscreen. 
         */
        if (event.type == KeyPress && event.xkey.subwindow != None) {
            if (event.xkey.keycode == C && event.xkey.state & MODKEY) killwindow(display, event.xkey.subwindow);
            if (event.xkey.keycode == F && event.xkey.state & MODKEY) XMoveResizeWindow(display, event.xkey.subwindow, 0, 0, xrrs[initial_size_id].width,  xrrs[initial_size_id].height);
        } 
        
        /* These next two functions were taken almost wholesale from MonsterWM. XGrabPointer takes a display, a subwindow to grab, a boolean as to whether the pointer events should be reported as usual, the event bitmask, the grab mode for the pointer and the keyboard, the window to confine it to (none for the root), the cursor to be displayed during grab, and the time. The time is passed in as a timestamp. 
         
         XGetwindowAttributes takes the display, the window whose attributes you want, and a pointer to fill with the XWindowAttrbiutes structure.
         
         */
        else if (event.type == ButtonPress && event.xbutton.subwindow != None) {
            XGrabPointer(display, event.xbutton.subwindow, True, PointerMotionMask | ButtonReleaseMask, GrabModeAsync,
                    GrabModeAsync, None, None, CurrentTime);
            XRaiseWindow(display, event.xbutton.subwindow);
            XSetInputFocus(display, event.xbutton.subwindow, RevertToPointerRoot, CurrentTime);
            XGetWindowAttributes(display, event.xbutton.subwindow, &win_attr);
            buttonevent = event.xbutton;
        } 
        

        else if (event.type == MotionNotify) {
            while (XCheckTypedEvent(display, MotionNotify, &event));
            XMoveResizeWindow(display, event.xmotion.window,
                    win_attr.x + (buttonevent.button == 1 ? event.xbutton.x_root - buttonevent.x_root : 0),
                    win_attr.y + (buttonevent.button == 1 ? event.xbutton.y_root - buttonevent.y_root : 0),
                    max(1, win_attr.width + (buttonevent.button == 3 ? event.xbutton.x_root - buttonevent.x_root : 0)),
                    max(1, win_attr.height + (buttonevent.button == 3 ? event.xbutton.y_root - buttonevent.y_root : 0)));
        } 
        
        /* Cleans up when the mouse button is released.  */
        else if (event.type == ButtonRelease)
            XUngrabPointer(display, CurrentTime);
    }
   
}
