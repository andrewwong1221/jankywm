This is my small window manager. Most of the standard features aren't yet implemented, or simply aren't going to be. This is my learning experience at how to tame the beast that is the Xorg API. 

Installation:

    sudo make install clean

Configuration:

    Set a background by using 
    
            feh --bg-scale "path/to/your/wallpaper"

    Set a cursor theme Xmonad style by placing this in your .xinitrc:

            xsetroot -cursor_name left_ptr

    Also, I don't feel like playing with xft libraries. That would more than triple my codebase. Fonts may be weird.  

Controls:

    Mod+x = Launch xterm
    Mod+F = Make Window Fullscreen
    Mod+C = Close window
    Mod and click/drag = Move the window
    Mod and right click/drag = Resize the window
    
    Where mod is defined by the macro at the header of the program. Mod4Mask is the default, which is the windows key.

    You can set the modkey and the keybindings by editing the GetSymKey macro calls.  
